import { Component, ViewChild } from '@angular/core';
import { ChallengeService } from 'src/challenge.service';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private service: ChallengeService) {}

  ngOnInit(){
    this.service.getChallengeData().subscribe((data)=>{
      console.log(data);
      this.dataSource = data;
      this.title = 'sms-fs-challenge-fe'
    });
  }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  title = '';
  dataSource:any = [];

}
