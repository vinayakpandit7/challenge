import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChallengeService {

  constructor(private http: HttpClient) {  }

  getChallengeData(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:3000/prices/');
  }
}
