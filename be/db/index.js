const mongoose = require('mongoose');
const { Schema } = mongoose;  

var CounterSchema = new Schema({
    _id: {type: String, required: true},
    seq: { type: Number, default: 0 }
});

var Counter = mongoose.model('Counter', CounterSchema, 'counters');

const priceDataSchema = new Schema({
    id: Number,
    city: String,
    start_date: Date,
    end_date: Date,
    price: Number,
    status: String,
    color: String
});

priceDataSchema.pre('save', function(next) {
    var doc = this;
    console.log('hiiiiiiiiiiiii')
    Counter.findByIdAndUpdate({_id: 'SEQUENCE_ID'}, {$inc: { seq: 1} }, {new: true, upsert: true}, function(error, counter)   { 
      if(error){
          return next(error);
      }else{
        doc.id = counter.seq;
        next();
      }
    });
});

const PriceDataModel = mongoose.model('PriceData', priceDataSchema, 'price_data');

module.exports = PriceDataModel;