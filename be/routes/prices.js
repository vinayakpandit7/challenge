var express = require('express');
var router = express.Router();
var PriceDataModel = require('../db/index');

router.get('/:id', function(req, res, next) {
  PriceDataModel.find({id: req.params.id},(err,data)=>{  
    res.send(data);
  })
  
});

router.get('/', function(req, res, next) {
  PriceDataModel.find({},(err,data)=>{  
    res.send(data);
  })
  
});

router.post('/', function(req, res, next) {
  PriceDataModel.create(req.body,(err,data)=>{  
    res.send(data);
  })
});

router.put('/:id', function(req, res, next) {
  PriceDataModel.updateOne({id:req.params.id}, req.body, (err,data)=>{  
    res.send(data);
  })
});

router.delete('/:id', function(req, res, next) {
  PriceDataModel.deleteOne({id:req.params.id},(err,data)=>{  
    res.send(data);
  })
});

module.exports = router;
